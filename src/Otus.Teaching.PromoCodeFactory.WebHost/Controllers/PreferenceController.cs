﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferenceController : Controller
    {
        private readonly IRepository<Preference> _preferenceRepository;

        public PreferenceController(
            IRepository<Preference> preferenceRepository)
        {
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить список всех предпочтений
        /// </summary>
        /// <returns>Список предпочтений</returns>
        [HttpGet]
        public async Task<ActionResult<PreferenceResponse>> GetCustomersAsync()
        {
            var preferences = await _preferenceRepository.GetAllAsync();

            var response = preferences.Select(p => new PreferenceResponse
            {
                Id = p.Id,
                Name = p.Name
            });

            return Ok(response);
        }
    }
}
