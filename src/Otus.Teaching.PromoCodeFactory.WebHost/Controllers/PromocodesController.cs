﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Customer> _customerRepository;

        public PromocodesController(
            IRepository<PromoCode> promoCodeRepository,
            IRepository<Preference> preferenceRepository,
            IRepository<Customer> customerRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _preferenceRepository = preferenceRepository;
            _customerRepository = customerRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promoCodes = await _promoCodeRepository.GetAllAsync();

            var response = promoCodes.Select(p => new PromoCodeShortResponse
            {
                BeginDate = p.BeginDate.ToString(),
                EndDate = p.EndDate.ToString(),
                Id = p.Id,
                Code = p.Code,
                PartnerName = p.PartnerName,
                ServiceInfo = p.ServiceInfo
            }).ToList();

            return response;
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preference = await _preferenceRepository.GetByIdAsync(request.PreferenceId);

            if (preference == null)
                return NoContent();

            var promoCode = PromoCodeMapper.MapGromModel(request, preference);

            await _promoCodeRepository.AddAsync(promoCode);

            var suitableCustomers = await _customerRepository
                .GetWhere(c => c.Preferences
                .Any(p => p.PreferenceId == preference.Id));

            foreach (var customer in suitableCustomers)
            {
                customer.PromoCodes.Add(new CustomerPromoCode
                {
                    Customer = customer,
                    CustomerId = customer.Id,
                    PromoCode = promoCode,
                    PromoCodeId = promoCode.Id
                });

                await _customerRepository.UpdateAsync(customer);
            }

            return Ok();
        }
    }
}