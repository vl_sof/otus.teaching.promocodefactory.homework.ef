﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContext
        : DbContext
    {
        public DbSet<Customer> Customers { get; set; }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Preference> Preferences { get; set; }

        public DbSet<PromoCode> PromoCodes { get; set; }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CustomerPreference>()
                .HasKey(cp => new { cp.CustomerId, cp.PreferenceId });
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(cp => cp.Customer)
                .WithMany(c => c.Preferences)
                .HasForeignKey(p => p.PreferenceId);
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(cp => cp.Preference)
                .WithMany()
                .HasForeignKey(c => c.CustomerId);

            modelBuilder.Entity<CustomerPromoCode>()
                .HasKey(cp => new { cp.CustomerId, cp.PromoCodeId });
            modelBuilder.Entity<CustomerPromoCode>()
                .HasOne(cp => cp.Customer)
                .WithMany(c => c.PromoCodes)
                .HasForeignKey(p => p.PromoCodeId);
            modelBuilder.Entity<CustomerPromoCode>()
                .HasOne(cp => cp.PromoCode)
                .WithMany()
                .HasForeignKey(p => p.PromoCodeId);
        }
    }
}