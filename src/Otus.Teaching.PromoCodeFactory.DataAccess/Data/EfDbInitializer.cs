﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly DataContext _dataContext;

        public EfDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void Initialize()
        {
            RecreateDb();
            AddFakeData();
        }

        private void RecreateDb()
        {
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();
        }

        private void AddFakeData()
        {
            _dataContext.Customers.AddRange(FakeDataFactory.Customers);
            _dataContext.Employees.AddRange(FakeDataFactory.Employees);

            _dataContext.SaveChangesAsync();
        }
    }
}
