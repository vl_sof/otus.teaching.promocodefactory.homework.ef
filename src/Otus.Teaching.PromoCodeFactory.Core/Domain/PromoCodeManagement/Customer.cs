﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        public Customer()
        {
            Preferences = new List<CustomerPreference>();
            PromoCodes = new List<CustomerPromoCode>();
        }

        [MaxLength(50)]
        public string FirstName { get; set; }

        [MaxLength(50)]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [MaxLength(50)]
        public string Email { get; set; }

        public virtual ICollection<CustomerPreference> Preferences { get; set; }
        
        public virtual ICollection<CustomerPromoCode> PromoCodes { get; set; }
    }
}